//
//  ERWKit.h
//  ERWKit
//
//  Created by Eryn Wells on 2015-03-28.
//  Copyright (c) 2015 Eryn Wells. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ERWKit.
FOUNDATION_EXPORT double ERWKitVersionNumber;

//! Project version string for ERWKit.
FOUNDATION_EXPORT const unsigned char ERWKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ERWKit/PublicHeader.h>


