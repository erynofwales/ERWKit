//
//  UIView+ERWKeyframeCurves.m
//  ERWKit
//
//  Created by Eryn Wells on 2015-03-28.
//  Copyright (c) 2015 Eryn Wells. All rights reserved.
//

#import "UIView+ERWKeyframeCurves.h"

static ERWKeyframeCurveBlock kLinearCurveBlock = ^(double frameTime)
{
    return frameTime;
};

static ERWKeyframeCurveBlock kEaseInOutCurveBlock = ^(double frameTime)
{
    return pow(frameTime, 1.5) / (pow(frameTime, 1.5) + pow(1.0 - frameTime, 1.5));
};

@implementation UIView (ERWKeyframeCurves)

+ (void)addKeyframesWithRelativeStartTime:(double)startTime
                         relativeDuration:(double)duration
                           numberOfFrames:(NSUInteger)numberOfFrames
                                    curve:(ERWKeyframeCurve)curve
                            keyframeBlock:(ERWKeyframeBlock)keyframeBlock
{
    ERWKeyframeCurveBlock curveBlock;
    switch (curve) {
        case ERWKeyframeCurveLinear:
            curveBlock = kLinearCurveBlock;
            break;
        case ERWKeyframeCurveEaseInOut:
            curveBlock = kEaseInOutCurveBlock;
            break;
        default:
            NSAssert(NO, @"Invalid keyframe curve specified");
            curveBlock = ERWKeyframeCurveLinear;
            break;
    }
    [UIView addKeyframesWithRelativeStartTime:startTime
                             relativeDuration:duration
                               numberOfFrames:numberOfFrames
                                   curveBlock:curveBlock
                                keyframeBlock:keyframeBlock];
}

+ (void)addKeyframesWithRelativeStartTime:(double)startTime
                         relativeDuration:(double)duration
                           numberOfFrames:(NSUInteger)numberOfFrames
                               curveBlock:(ERWKeyframeCurveBlock)curveBlock
                            keyframeBlock:(ERWKeyframeBlock)keyframeBlock
{
    double keyframeDuration = duration / numberOfFrames;
    for (NSUInteger i = 0; i < numberOfFrames; i++) {
        double keyframeStartTime = i * keyframeDuration;
        [UIView addKeyframeWithRelativeStartTime:startTime + keyframeStartTime
                                relativeDuration:keyframeDuration
                                      animations:^{
                                          if (curveBlock && keyframeBlock) {
                                              keyframeBlock(curveBlock(keyframeStartTime));
                                          }
                                      }];
    }
}

@end
