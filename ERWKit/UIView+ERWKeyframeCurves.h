//
//  UIView+ERWKeyframeCurves.h
//  ERWKit
//
//  Created by Eryn Wells on 2015-03-28.
//  Copyright (c) 2015 Eryn Wells. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ERWKeyframeCurve) {
    ERWKeyframeCurveLinear,
    ERWKeyframeCurveEaseInOut,
};

typedef double (^ERWKeyframeCurveBlock)(double frameTime);
typedef void (^ERWKeyframeBlock)(double frameTime);


@interface UIView (ERWKeyframeCurves)

+ (void)addKeyframesWithRelativeStartTime:(double)duration
                         relativeDuration:(double)duration
                           numberOfFrames:(NSUInteger)numberOfFrames
                                    curve:(ERWKeyframeCurve)curve
                            keyframeBlock:(ERWKeyframeBlock)keyframeBlock;

/**
 * Generate `numberOfFrames` keyframes for an animation by spacing them out along the curve defined by `curveBlock`. For
 * each keyframe, `keyframeBlock` is called to compute the animation for that frame.
 *
 * @param [in] startTime The starting time of the set of keyframes, relative to the start of the animation. This should 
 *                       be a floating point value between 0.0 and 1.0.
 * @param [in] duration The total duration of the generated keyframes. Each keyframe will be spaced evenly over the 
 *                      duration.
 * @param [in] numberOfFrames The total number of frames to generate. The duration of the animation will be subdivided 
 *                            into this many keyframes.
 * @param [in] curveBlock A block defining the curve of the animation.
 * @param [in] keyframeBlock A block that should compute the animation for the given frame.
 */
+ (void)addKeyframesWithRelativeStartTime:(double)startTime
                         relativeDuration:(double)duration
                           numberOfFrames:(NSUInteger)numberOfFrames
                               curveBlock:(ERWKeyframeCurveBlock)curveBlock
                            keyframeBlock:(ERWKeyframeBlock)keyframeBlock;

@end
