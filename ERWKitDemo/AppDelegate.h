//
//  AppDelegate.h
//  ERWKitDemo
//
//  Created by Eryn Wells on 2015-03-29.
//  Copyright (c) 2015 Eryn Wells. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

