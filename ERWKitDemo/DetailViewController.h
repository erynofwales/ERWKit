//
//  DetailViewController.h
//  ERWKitDemo
//
//  Created by Eryn Wells on 2015-03-29.
//  Copyright (c) 2015 Eryn Wells. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

